import ssl
import yaml
import smtplib
from email.message import EmailMessage


def send_message(body: str, subject: str, to: list[str], config):
    username, password = [config[k] for k in ['username', 'password']]
    msg = EmailMessage()
    msg.set_content(body)
    msg['Subject'] = subject
    msg['From'] = username
    msg['To'] = to
    
    context = ssl.create_default_context()
    with smtplib.SMTP('smtp.office365.com', port=587) as con:
        con.starttls(context=context)
        con.login(username, password)
        con.send_message(msg)


if __name__ == '__main__':
    with open('config.yaml') as f:
        config = yaml.safe_load(f)

    body = 'Test!'
    subject = 'Test?'
    toaddr = ['szagrobelny@saveonsp.com']

    send_message(body, subject, toaddr, config)
